package com.gxa.controller;

import com.gxa.dto.MyParam;
import com.gxa.dto.ResultDto;


import com.gxa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author lqz
 * @Data: 2021/3/17
 */

@Controller
@RequestMapping("/member")
public class AdminController {

    @Autowired
    private UserService userService;

    @GetMapping("/list/page")
    public String adminList(){
        return "/admin/admin-list";
    }

    @GetMapping("/add/page")
    public String adminAdd(){
        return "/admin/admin-add";
    }

    /**
     * 列表数据
     * @param param
     * @return
     */
    @PostMapping("/list/do")
    @ResponseBody
    public ResultDto list(MyParam param){
        return userService.list(param);
    }


    /**
     * 删除数据
     * @param userId
     * @return
     */
    @PostMapping("/delete")
    @ResponseBody
    public ResultDto delete(Integer userId){
        return userService.delete(userId);
    }
}
