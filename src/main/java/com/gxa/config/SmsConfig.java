package com.gxa.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:config/sms.properties")
@Data
public class SmsConfig {

    @Value("${sms.serverIp}")
    private String serverIp;
    @Value("${sms.serverPort}")
    private String serverPort;
    @Value("${sms.accountSId}")
    private String accountSId;
    @Value("${sms.accountToken}")
    private String accountToken;
    @Value("${sms.appId}")
    private String appId;
    @Value("${sms.templateId}")
    private String templateId;

}
