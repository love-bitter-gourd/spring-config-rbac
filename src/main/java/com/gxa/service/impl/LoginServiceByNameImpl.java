package com.gxa.service.impl;

import com.gxa.dto.ResultDto;
import com.gxa.exception.SystemException;
import com.gxa.group.LoginName;
import com.gxa.mapper.AuthMapper;
import com.gxa.mapper.UserMapper;
import com.gxa.pojo.Auth;
import com.gxa.pojo.Manu;
import com.gxa.pojo.Role;
import com.gxa.pojo.User;
import com.gxa.service.LoginService;
import com.gxa.util.MD5Util;
import com.gxa.util.Response;
import com.gxa.validator.MyValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Service("nameLoginService")
public class LoginServiceByNameImpl implements LoginService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private MyValidator validator;
    @Autowired
    private AuthMapper authMapper;


    @Override
    public ResultDto login(User user, HttpSession session) {
        // 校验
        validator.validate(user, LoginName.class);
        // 查询数据库
        User dbUser = userMapper.findByName(user.getUserName());
        if (dbUser == null) {
            throw new SystemException(1003, "该用户不存在！");
        }
        // 判断密码
        if (!dbUser.getUserPassword().equals(MD5Util.MD55(user.getUserPassword()))) {
            throw new SystemException(1004, "密码错误！");
        }
        // 登录成功
        session.setAttribute("userName", dbUser.getUserName());

        //权限
        //先判断是否超级管理员
        Boolean isSuper = isSuper(dbUser);
        session.setAttribute("isSuper",isSuper);
        List<Manu> manus = null;
        if(isSuper) {
            List<Auth> allManu = authMapper.findAllManu();
             manus = getManu(allManu);
        }else {
            List<Auth> auths = getAuth(dbUser);
            manus = getManu(auths);
            // 处理权限数据
            session.setAttribute("authList", auths);
        }
        session.setAttribute("manus",manus);
        return Response.success("登录成功！");
    }


    /**
     * 从用户中去获取权限列表
     * @param user
     * @return
     */
    private List<Auth> getAuth(User user){
        // 权限容器
        List<Auth> authList = new ArrayList<>();
        List<Role> roles = user.getRoles();
        for (Role role : roles) {
            authList.addAll(role.getAuthList());
        }
        return authList;
    }

    /**
     * 判断是否是超级管理员
     * @param user
     * @return
     */
    public Boolean isSuper(User user){
        List<Role> roles = user.getRoles();
        if (roles == null){
            return false;
        }
        for (Role role : roles) {
            if (role == null || role.getIsSuper() == null){
                continue;
            }
            if (role.getIsSuper().equals(1)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 设置菜单
     * @param auths
     * @return
     */
    public List<Manu> getManu(List<Auth> auths){

        List<Manu> manus = new ArrayList<Manu>();
        for (Auth auth : auths) {
            if (auth.getAuthLevel().equals(1)&&auth.getIsMenu().equals(1)) {
                manus.add(new Manu(auth.getAuthName(),auth.getAuthUrl(),auth.getAuthLevel(),auth.getAuthId(),null));
            }
        }
        for (Manu manu : manus) {
            List<Manu> subManu = new ArrayList<>();
            for (Auth auth : auths) {
                if (auth.getAuthLevel().equals(2)&&auth.getIsMenu().equals(1)) {
                    if(manu.getMenuId().equals(auth.getAuthParentId())) {
                        subManu.add(new Manu(auth.getAuthName(), auth.getAuthUrl(), auth.getAuthLevel(), auth.getAuthId(), null));
                    }
                }
            }
            manu.setSubMenu(subManu);

        }
        return manus;
    }
}
