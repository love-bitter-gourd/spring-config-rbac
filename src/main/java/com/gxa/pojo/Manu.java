package com.gxa.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @Author lqz
 * @Data: 2021/3/17
 */
@Data
@AllArgsConstructor
public class Manu {

    /**
     * 菜单的名字
     */
    private String menuName;

    /**
     * 菜单跳转地址
     */
    private String menuUrl;

    /**
     * 菜单等级
     */
    private Integer menuLevel;

    /**
     * 菜单ID
     */
    private Integer menuId;

    /**
     * 子菜单
     */
    private List<Manu> subMenu;
}
