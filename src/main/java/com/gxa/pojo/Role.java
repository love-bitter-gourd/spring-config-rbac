package com.gxa.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * role
 * @author 
 */
@Data
public class Role implements Serializable {
    private Integer roleId;

    /**
     * 角色名字
     */
    private String roleName;

    /**
     * 角色描述
     */
    private String roleDesc;

    /**
     * 0 不是 1 是
     */
    private Integer isSuper;

    private static final long serialVersionUID = 1L;

    List<Auth> authList;
}