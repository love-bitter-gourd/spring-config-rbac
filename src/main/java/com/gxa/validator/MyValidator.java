package com.gxa.validator;

import com.gxa.exception.SystemException;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

@Component
public class MyValidator {

    /**
     * 验证器
     */
    private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    /**
     * 不带分组的校验
     * @param obj
     */
    public void validate(Object obj){
        Set<ConstraintViolation<Object>> validate = validator.validate(obj);
        for (ConstraintViolation<Object> violation : validate) {
            throw new SystemException(1003, violation.getMessage());
        }
    }

    /**
     * 带分组的校验
     * @param obj
     */
    public void validate(Object obj, Class<?> clazz){
        Set<ConstraintViolation<Object>> validate = validator.validate(obj, clazz);
        for (ConstraintViolation<Object> violation : validate) {
            throw new SystemException(1003, violation.getMessage());
        }
    }



}
