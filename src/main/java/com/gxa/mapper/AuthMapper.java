package com.gxa.mapper;

import com.gxa.pojo.Auth;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AuthMapper {
    int deleteByPrimaryKey(Integer authId);

    int insert(Auth record);

    int insertSelective(Auth record);

    Auth selectByPrimaryKey(Integer authId);

    int updateByPrimaryKeySelective(Auth record);

    int updateByPrimaryKey(Auth record);

    List<Auth> findAllManu();

    /**
     * 通过角色查询权限
     * @param roleId
     * @return
     */
    List<Auth> findByRoleId(@Param("roleId") Integer roleId);
}