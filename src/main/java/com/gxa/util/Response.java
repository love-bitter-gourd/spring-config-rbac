package com.gxa.util;

import com.gxa.dto.ResultDto;

/**
 * 响应类
 * @author 一路向北
 */
public class Response {

    /**
     * 成功的响应
     * @return
     */
    public static ResultDto success(){
        return new ResultDto(200, "success!");
    }

    /**
     * 成功的响应
     * @return
     */
    public static ResultDto success(String msg){
        return new ResultDto(200, msg);
    }

    /**
     * 成功的响应
     * @return
     */
    public static ResultDto success(Integer code, String msg){
        return new ResultDto(code, msg);
    }

    /**
     * 成功的响应
     * @return
     */
    public static ResultDto success(Integer code, String msg, Object data){
        return new ResultDto(code, msg, data);
    }


    /**
     * 成功的响应
     * @return
     */
    public static ResultDto success(Integer code, String msg,Integer count, Object data){
        return new ResultDto(code, msg,count, data);
    }


    /**
     *  错误的响应
     * @param code
     * @param msg
     * @return
     */
    public static ResultDto error(Integer code, String msg){
        return new ResultDto(code, msg);
    }


}
