package com.gxa.dto;

import lombok.Data;

/**
 * 接受参数实体
 */
@Data
public class MyParam {

    /**
     * 当前页码
     */
    private Integer pageNum = 1;
    /**
     * 页容量
     */
    private Integer pageSize = 3;


    private Integer page = 1;

    private Integer limit = 10;

    /**
     * 搜索的关键字
     */
    private String keywords;

}
